<?php
/**
 * Amstelodamum back compat functionality
 *
 * Prevents Amstelodamum from running on WordPress versions prior to 4.2,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.2.
 *
 * @package WordPress
 * @subpackage Amstelodamum
 * @since Amstelodamum 1.0
 */

/**
 * Prevent switching to Amstelodamum on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Amstelodamum 1.0
 */
function amstelodamum_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'amstelodamum_upgrade_notice' );
}
add_action( 'after_switch_theme', 'amstelodamum_switch_theme' );

/**
 * Add message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Amstelodamum on WordPress versions prior to 4.2.
 *
 * @since Amstelodamum 1.0
 */
function amstelodamum_upgrade_notice() {
	$message = sprintf( esc_html__( 'Amstelodamum requires at least WordPress version 4.2. You are running version %s. Please upgrade and try again.', 'amstelodamum' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevent the Customizer from being loaded on WordPress versions prior to 4.2.
 *
 * @since Amstelodamum 1.0
 */
function amstelodamum_customize() {
	wp_die( sprintf( esc_html__( 'Amstelodamum requires at least WordPress version 4.2. You are running version %s. Please upgrade and try again.', 'amstelodamum' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'amstelodamum_customize' );

/**
 * Prevent the Theme Preview from being loaded on WordPress versions prior to 4.2.
 *
 * @since Amstelodamum 1.0
 */
function amstelodamum_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( esc_html__( 'Amstelodamum requires at least WordPress version 4.2. You are running version %s. Please upgrade and try again.', 'amstelodamum' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'amstelodamum_preview' );
