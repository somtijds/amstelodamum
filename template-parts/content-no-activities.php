<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Amstelodamum
 * @since Amstelodamum 1.0
 */
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Geen activiteiten gevonden', 'amstelodamum' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
        <?php $archiveUrl = get_post_type_archive_link('activity'); ?>
        <?php if (!empty( $archiveUrl)) :
            $archiveLink = '<a href="' . esc_url($archiveUrl) . '">' . esc_html__('activiteiten-archief','amstelodamum') . '</a>'; ?>
        <p><?php echo sprintf(
                esc_html__('Er zijn momenteel geen activiteiten gepland. Hier is een link naar het activiteiten die in het verleden zijn georganiseerd: %s.', 'amstelodamum'),
                $archiveLink
            ); ?>
        </p>
        <?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
