<?php
/**
 * Template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Amstelodamum
 * @since Amstelodamum 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-summary">
	<?php
		// start en einddatum hetzelfde
		if ( get_field( 'start_date' ) == get_field( 'end_date' ) OR trim( get_field( 'end_date' ) ) == false ) {
			$dateformatstring = "l j F Y";
			$unixtimestamp = strtotime(get_field('start_date'));
			echo "<div>" , ucfirst( date_i18n( $dateformatstring, $unixtimestamp ) ) , "</div>";
	
			// starttijd niet leeg
			if ( ! trim( get_field( 'start_time' ) ) == false ) {				
				// start en eindtijd hetzelfde
				if ( get_field( 'start_time' ) == get_field( 'end_time' ) OR trim( get_field( 'end_time' ) ) == false ) {
					echo "<div>" , _( "Om " ) , the_field( 'start_time' ) , " uur</div>";
				} else {
					echo "<div>Van " , the_field( 'start_time' ) , " tot " , the_field( 'end_time' ) , " uur</div>";
				}
			}
			
		// start en einddatum zelfde maand en jaar					
		} else  if ( substr( get_field( 'start_date' ), 0, 6 ) == substr( get_field( 'end_date' ), 0, 6 )  ) {
			$dateformatstring = "l j F";
			$unixtimestamp = strtotime(get_field('start_date'));
			echo "<div>" , ucfirst( date_i18n( $dateformatstring, $unixtimestamp ) ), " - ";
			$dateformatstring = "l j F Y";
			$unixtimestamp = strtotime(get_field('end_date'));
			echo date_i18n( $dateformatstring, $unixtimestamp ) , "</div>";
	
		// start en einddatum zelfde jaar					
		} else  if ( substr( get_field( 'start_date' ), 0, 4 ) == substr( get_field( 'end_date' ), 0, 4 )  ) {
			$dateformatstring = "l j F";
			$unixtimestamp = strtotime(get_field('start_date'));
			echo "<div>" , ucfirst( date_i18n( $dateformatstring, $unixtimestamp ) ), " - ";
			$dateformatstring = "l j F Y";
			$unixtimestamp = strtotime(get_field('end_date'));
			echo date_i18n( $dateformatstring, $unixtimestamp ) , "</div>";
	
		// start en einddatum verschillend jaar					
		} else {
			$dateformatstring = "l j F Y";
			$unixtimestamp = strtotime(get_field('start_date'));
			echo "<div>" , ucfirst( date_i18n( $dateformatstring, $unixtimestamp ) ), " - ";
			$dateformatstring = "l j F Y";
			$unixtimestamp = strtotime(get_field('end_date'));
			echo date_i18n( $dateformatstring, $unixtimestamp ) , "</div>";					
		}	
		
		if ( ! trim( get_field( 'location_name' ) ) == false ) {
			echo "<div>" , the_field( 'location_name' ) , "</div>";
		}
		
		if ( ! trim( get_field( 'location_address' ) ) == false ) {
			echo "<div>" , the_field( 'location_address' );
				
			if ( ( ! trim( get_field( 'location_city' ) ) == false ) AND ( get_field( 'location_city' ) !== 'Amsterdam' ) ) {
				echo ", " , the_field( 'location_city' ) , "</div>";
			} else {
				echo "</div>";
			}
		}
	?>
	</div>

	<?php if ( has_excerpt() ) { ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
	<?php } ?>

	<?php amstelodamum_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'amstelodamum' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'amstelodamum' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' != get_the_author_meta( 'description' ) ) :
				get_template_part( 'template-parts/biography' );
			endif;
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php amstelodamum_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'amstelodamum' ),
					the_title( '<span class="screen-reader-text">', '</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
