<?php
/**
 * The template used for displaying page content that needs an iframe for a form
 *
 * @package WordPress
 * @subpackage Amstelodamum
 * @since Amstelodamum 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php amstelodamum_post_thumbnail(); ?>

	<div class="entry-content">

		<?php $iframe_url = get_post_meta( get_the_ID(), 'url_iframe_form', true ); ?>
		<?php if ( ! empty( $iframe_url ) ) : ?>

			<iframe id="formulier" src="<?php echo esc_url( $iframe_url ); ?>" width="100%" height="800" frameborder="0" scrolling="no"></iframe>

		<?php endif; ?>

	</div><!-- .entry-content -->

	<?php
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				esc_html__( 'Edit %s', 'amstelodamum' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			),
			'<footer class="entry-footer"><span class="edit-link">',
			'</span></footer><!-- .entry-footer -->'
		);
	?>

</article><!-- #post-## -->
