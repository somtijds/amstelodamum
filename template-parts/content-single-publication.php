<?php
/**
 * Template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Amstelodamum
 * @since Amstelodamum 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
			if ( get_field( 'publication_type' ) == "Maandblad" ) :
				echo '<h1 class="entry-title">', the_field( 'publication_type' ), '<br />', the_field( 'publication_months' ), ' ', the_field( 'publication_year' ), '</h1>';
			else :
				echo '<h1 class="entry-title">', the_field( 'publication_type' ), ' ', the_field( 'publication_year' ), '</h1>' ;
			endif;
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		
			$publication_cover = get_field('publication_cover');
		
			if ( $publication_cover ) :
				echo '<div class="publication-cover">', wp_get_attachment_image( $publication_cover, 'large' ), '</div>';
			endif;

			if ( get_field( 'publication_type' ) == "Maandblad" ) :		
				echo '<p><i>Jaargang ', the_field( 'publication_volume' ), ' (', the_field( 'publication_year' ), ') nummer ', the_field( 'publication_issue' ), '</i><br />';
			else :
				echo '<p><i>', the_title(), '</i><br />';
			endif;
			
			if ( get_field( 'publication_link' ) ) :
				echo '<a href="', the_field( 'publication_link' ), '">Bestel deze uitgave</a></p>';
			endif;
		
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'amstelodamum' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'amstelodamum' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' != get_the_author_meta( 'description' ) ) :
				get_template_part( 'template-parts/biography' );
			endif;
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php amstelodamum_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'amstelodamum' ),
					the_title( '<span class="screen-reader-text">', '</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
