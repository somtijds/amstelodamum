��    R      �  m   <      �  
   �     �     	          +  o   <  8   �     �                     0     A     R     f     k     s     {     �     �     �  E   �  W   �  =   L	  
   �	     �	     �	     �	     �	  	   �	     �	     
  
   
  	   
  
   
     &
     8
     J
     X
     g
  %   k
     �
     �
     �
  a   �
  #     "   9  "   \  "        �     �     �     �     �     �  
   �            F   $     k     �     �     �     �     �     �  \   �     9  &   @  0   g  $   �     �  #   �       =        [     p     w  T   �     �     �  m  	  
   w     �     �     �     �  m   �  D   3  	   x     �     �     �      �      �     �       	          
   &     1     7     =  U   A  _   �  D   �  
   <     G     V     [     _     c     p     �     �     �     �     �     �     �     �     	  %        3     :  	   I  S   S     �     �  
   �  
   �     �     �     �               (  
   C     N     Z  I   l     �     �  	   �     �                5  S   ;     �     �     �     �     �  	   �     �     �     �     �     �  ?   �     ?     R         >       F      M                 +       ,      D          O         9   /   1   8   K   &   -   @      "       R       0         
         B      2   6      %          )           ;   =                     3   Q      *                  5   P      I   G           '   #              ?   $   C      N   	          :       7   J                     4   A      L           E      H      .   !   (   <    Activiteit Activiteiten Activity Widget Alle activiteiten Alle publicaties Amstelodamum requires at least WordPress version 4.2. You are running version %s. Please upgrade and try again. Appears at the bottom of the content on posts and pages. Appears at the front page. Author: Comment navigation Comments are closed. Content Bottom 1 Content Bottom 2 Continue reading %s Dark Default Edit %s Featured Gray Green Inconsolata font: on or offon It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment<span class="screen-reader-text"> on %s</span> Link Color Main Text Color Menu Merriweather font: on or offon Montserrat font: on or offon New title Newer Comments Next Next Image Next page Next post: Nieuwe activiteit Nieuwe publicatie Nothing Found Older Comments Om  Oops! That page can&rsquo;t be found. Page Page Background Color Pages: Parent post link<span class="meta-nav">Published in</span><span class="post-title">%title</span> Post Type General NameActiviteiten Post Type General NamePublicaties Post Type Singular NameActiviteit Post Type Singular NamePublicatie Previous Previous Image Previous page Previous post: Primary Menu Proudly powered by %s Publicatie Publicaties Publication Widget Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Results for: %s Secondary Text Color Sidebar Sidebar Left Sidebar Right Skip to content Social Links Menu Sorry, but nothing matched your search terms. Please try again with some different keywords. Title: Used before category names.Categories Used before full size attachment link.Full size Used before post author name.Author Used before post format.Format Used before publish date.Posted on Used before tag names.Tags Used between list items, there is a space after the comma.,  View all posts by %s Yellow collapse child menu comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; expand child menu https://wordpress.org/ Project-Id-Version: Amstelodamum
POT-Creation-Date: 2019-04-12 15:12+0200
PO-Revision-Date: 2019-04-12 15:12+0200
Last-Translator: 
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Activiteit News & Events Activiteit Widget Alle activiteiten Alle publicaties Amstelodamum vereist minimaal WordPress versie 4.2. U werkt met versie %s. Upgrade nu en probeer het opnieuw. Verschijnt aan de onderkant van de inhoud van berichten en pagina's. Home Page Auteur: Reactie-navigatie Reageren is niet mogelijk Sla dit over; ga naar de inhoud. Sla dit over; ga naar de inhoud. Verder lezen %s Donker Standaard %s aanpassen Uitgelicht Grijs Groen aan Het lijkt erop dat deze pagina niet (meer) bestaat. Misschien helpt een zoekopdracht? Het lijkt erop dat we niet kunnen vinden wat je zoekt. Misschien kan de zoekfunctie wel helpen. Laat een reactie achter <span class="screen-reader-text">op%s</span> Link kleur Het Hoofd Menu Menu aan aan Nieuwe titel Nieuwe reacties volgend bericht Volgende afbeelding Volgende pagina volgend bericht: Nieuwe activiteit Nieuwe publicatie Niet gevonden! Eerdere Reacties Ohm Deze pagina kan niet worden gevonden. Pagina Niet gevonden! Pagina's: <span class="meta-nav">Gepubliceerd in</span><span class="post-title">%title</span> News & Events Publicaties Activiteit Publicatie &larr; vorig bericht Vorige afbeelding Vorige pagina vorig bericht: Primair Menu Deze website draait op %s. Publicatie Publicaties Publicatie Widget Klaar om je eerste bericht te publiceren? <a href="%1$s">Begin hier</a> . Zoekresultaten voor: %s Secundaire tekstkleur Zijbalk 1 Zijbalk uitklappen Zijbalk uitklappen Sla dit over; ga naar de inhoud. Links Geen resultaten gevonden met deze zoekwoorden. Probeer eens met andere trefwoorden. Titel: Categorieën Volledige grootte auteur Format Geplaatst Tags , Toon alle berichten in %s Geel Mobiel menu Één reactie op “%2$s” %1$s reacties op &ldquo;%2$s&rdquo; Zijbalk uitklappen https://wordpress.org/ 