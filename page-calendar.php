<?php
/**
 * Template Name: Activiteiten-kalender
 *
 * The second template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package Amstelodamum
 * @since 	1.0.0
 * @version	1.0.0
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $today = date('Ymd');
        $query_args = array(
            'post_type' => 'activity',
            'paged' => $paged,
            'posts_per_page' => '4',
            'meta_key' => 'start_date',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => [
                [
                    'key' => 'start_date'
                ],
                [
                    'key' => 'start_date',
                    'value' => $today,
                    'compare' => '>='
                ]
            ]
        );

        query_posts( $query_args );

        if ( have_posts() ) : ?>

            <header class="page-header">
                <h1 class="page-title">Activiteiten</h1>
            </header><!-- .page-header -->

            <?php
            // Start the Loop.

            while ( have_posts()) : the_post();

                /*
                 * Include the Post-Format-specific template for the content.
                 * If you want to override this in a child theme, then include a file
                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                 */
                get_template_part( 'template-parts/content', 'single-activity' );

            endwhile;

            if ( have_posts() ) :
                // Previous/next page navigation.
                the_posts_pagination( array(
                    'prev_text'          => esc_html__( 'Previous page', 'amstelodamum' ),
                    'next_text'          => esc_html__( 'Next page', 'amstelodamum' ),
                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'amstelodamum' ) . ' </span>',
                ) );
            endif;

        // If no content, include the "No posts found" template.
        else :
            get_template_part( 'template-parts/content', 'no-activities' );

        endif;
        ?>

    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
