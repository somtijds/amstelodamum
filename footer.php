<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Amstelodamum
 * @since Amstelodamum 1.0
 */
?>

		</div><!-- .site-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<nav class="main-navigation" role="navigation">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'primary-menu'
						 ) );
					?>
				</nav><!-- .main-navigation -->
			<?php endif; ?>

			<div class="site-info">
				<?php
					/**
					 * Fires before the amstelodamum footer text for footer customization.
					 *
					 * @since Amstelodamum 1.0
					 */
					do_action( 'amstelodamum_credits' );
				?>
				<span class="site-title--footer"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?></a></span>
				<a class="site-title--footer" href="<?php echo esc_url( __( 'https://wordpress.org/', 'amstelodamum' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'amstelodamum' ), 'WordPress' ); ?></a>
				<?php $privacy_page = get_option( 'wp_page_for_privacy_policy '); ?>
			<?php if ( ! empty( $privacy_page ) && 'publish' === get_post_status( $privacy_page ) ) : ?>
			<span class="link-to-privacy-statement">
				<a href="<?php echo esc_url( get_the_permalink( $privacy_page ) ); ?>" title="<?php echo esc_attr( get_the_title( $privacy_page ) ); ?>"><?php echo esc_html( get_the_title( $privacy_page ) ); ?></a>
			</span>
			<?php endif; ?>
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
