<?php
/**
 * Amstelodamum functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Amstelodamum
 * @since Amstelodamum 1.0
 */

/**
 * Amstelodamum only works in WordPress 4.2 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.2', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'amstelodamum_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function amstelodamum_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Amstelodamum, use a find and replace
	 * to change 'amstelodamum' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'amstelodamum', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 0, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'amstelodamum' ),
		'social'  => esc_html__( 'Social Links Menu', 'amstelodamum' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', amstelodamum_fonts_url() ) );
}
endif; // amstelodamum_setup
add_action( 'after_setup_theme', 'amstelodamum_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function amstelodamum_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'amstelodamum_content_width', 840 );
}
add_action( 'after_setup_theme', 'amstelodamum_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function amstelodamum_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Left', 'amstelodamum' ),
		'id'            => 'sidebar-left',
		'description'   => esc_html__( 'Appears at the front page.', 'amstelodamum' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Right', 'amstelodamum' ),
		'id'            => 'sidebar-right',
		'description'   => esc_html__( 'Appears at the front page.', 'amstelodamum' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'amstelodamum' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Content Bottom 1', 'amstelodamum' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Appears at the bottom of the content on posts and pages.', 'amstelodamum' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Content Bottom 2', 'amstelodamum' ),
		'id'            => 'sidebar-3',
		'description'   => esc_html__( 'Appears at the bottom of the content on posts and pages.', 'amstelodamum' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'amstelodamum_widgets_init' );

if ( ! function_exists( 'amstelodamum_fonts_url' ) ) :
/**
 * Register Google fonts for Amstelodamum.
 *
 * @since Amstelodamum 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function amstelodamum_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== esc_html_x( 'on', 'Merriweather font: on or off', 'amstelodamum' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== esc_html_x( 'on', 'Montserrat font: on or off', 'amstelodamum' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== esc_html_x( 'on', 'Inconsolata font: on or off', 'amstelodamum' ) ) {
		$fonts[] = 'Inconsolata:400';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Amstelodamum 1.0
 */
function amstelodamum_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'amstelodamum_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 */
function amstelodamum_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'amstelodamum-fonts', amstelodamum_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.3' );

	wp_enqueue_style( 'amstelodamum-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'amstelodamum-ie', get_template_directory_uri() . '/assets/css/ie.css', array( 'amstelodamum-style' ), '20150905' );
	wp_style_add_data( 'amstelodamum-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'amstelodamum-ie7', get_template_directory_uri() . '/assets/css/ie7.css', array( 'amstelodamum-style' ), '20150905' );
	wp_style_add_data( 'amstelodamum-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'amstelodamum-html5', get_template_directory_uri() . '/assets/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'amstelodamum-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'amstelodamum-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20150905', true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'amstelodamum-keyboard-image-navigation', get_template_directory_uri() . '/assets/js/keyboard-image-navigation.js', array( 'jquery' ), '20150905' );
	}

	if ( is_page_template('page-form.php') ) {
		wp_enqueue_script( 'amstelodamum-iframe-resize', get_template_directory_uri() . '/vendor/iframe-resizer/js/iframeResizer.min.js', array(), '', true );
		wp_enqueue_script( 'amstelodamum-script', get_template_directory_uri() . '/assets/js/functions.js', array( 'jquery','amstelodamum-iframe-resize' ), '20160915', true );
	} else {
		wp_enqueue_script( 'amstelodamum-script', get_template_directory_uri() . '/assets/js/functions.js', array( 'jquery' ), '20160914', true );
	}


	wp_localize_script( 'amstelodamum-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . esc_html__( 'expand child menu', 'amstelodamum' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . esc_html__( 'collapse child menu', 'amstelodamum' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'amstelodamum_scripts' );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since Amstelodamum 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function amstelodamum_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'amstelodamum_search_form_modify' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function amstelodamum_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'amstelodamum_body_classes' );

/**
 * Convert HEX to RGB.
 *
 * @since Amstelodamum 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function amstelodamum_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) == 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) == 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


/**
 * Register post types
 */
function amstelodamum_post_types() {
	register_post_type( 'publication', array(
		'labels' => array(
			'name'                => _x( 'Publicaties', 'Post Type General Name', 'amstelodamum' ),
			'singular_name'       => _x( 'Publicatie', 'Post Type Singular Name', 'amstelodamum' ),
			'menu_name'           => __( 'Publicaties', 'amstelodamum' ),
			'name_admin_bar'      => __( 'Publicatie', 'amstelodamum' ),
			'all_items'           => __( 'Alle publicaties', 'amstelodamum' ),
			'add_new_item'        => __( 'Nieuwe publicatie', 'amstelodamum' ),
			'add_new'             => __( 'Nieuwe publicatie', 'amstelodamum' ),
			'new_item'            => __( 'Nieuwe publicatie', 'amstelodamum' ) ),
		'public'  => true,
		'has_archive' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array(
			'slug' => 'publicaties',
			'feeds' => false
        ),
		'query_var' => false,
		'delete_with_user' => false,
		'supports' => array( 'title', 'editor' ),
	) );
	register_post_type( 'activity', array(
		'labels' => array(
			'name'                => _x( 'Activiteiten', 'Post Type General Name', 'amstelodamum' ),
			'singular_name'       => _x( 'Activiteit', 'Post Type Singular Name', 'amstelodamum' ),
			'menu_name'           => __( 'Activiteiten', 'amstelodamum' ),
			'name_admin_bar'      => __( 'Activiteit', 'amstelodamum' ),
			'all_items'           => __( 'Alle activiteiten', 'amstelodamum' ),
			'add_new_item'        => __( 'Nieuwe activiteit', 'amstelodamum' ),
			'add_new'             => __( 'Nieuwe activiteit', 'amstelodamum' ),
			'new_item'            => __( 'Nieuwe activiteit', 'amstelodamum' ) ),

		'public' => true,
		'has_archive' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'activiteiten-archief'),
		'query_var' => false,
		'delete_with_user' => false,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
	) );
}

add_action( 'init', 'amstelodamum_post_types' );


/**
 * Add Activity_Widget widget
 */
class Activity_Widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'activity_widget', // Base ID
			__( 'Activiteiten', 'amstelodamum' ), // Name
			array( 'description' => __( 'Activity Widget', 'amstelodamum' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}

		$today = date( 'Ymd' );

		$query_args = array(
			'post_type' => 'activity',
			'posts_per_page' => -1,
			'meta_key' => 'start_date',
			'orderby' => 'meta_value_num',
			'order' => 'ASC',
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'start_date',
					'compare' => '>=',
					'value' => $today,
				),
				array(
					'key' => 'end_date',
					'compare' => '>=',
					'value' => $today,
				),
			),
		);

		$the_query = new WP_Query( $query_args );

		while ($the_query -> have_posts()) : $the_query -> the_post();
			echo "<a href=\"" , the_permalink() , "\" >" , the_title() , "</a>";

			// start en einddatum hetzelfde
			if ( get_field( 'start_date' ) == get_field( 'end_date' ) OR trim( get_field( 'end_date' ) ) == false ) {
				$dateformatstring = "l j F Y";
				$unixtimestamp = strtotime(get_field('start_date'));
				echo "<div>" , ucfirst( date_i18n( $dateformatstring, $unixtimestamp ) ) , "</div>";

				// starttijd niet leeg
				if ( ! trim( get_field( 'start_time' ) ) == false ) {
					// start en eindtijd hetzelfde
					if ( get_field( 'start_time' ) == get_field( 'end_time' ) OR trim( get_field( 'end_time' ) ) == false ) {
						echo "<div>Om " , the_field( 'start_time' ) , " uur</div>";
					} else {
						echo "<div>Van " , the_field( 'start_time' ) , " tot " , the_field( 'end_time' ) , " uur</div>";
					}
				}

			// start en einddatum zelfde maand en jaar
			} else  if ( substr( get_field( 'start_date' ), 0, 6 ) == substr( get_field( 'end_date' ), 0, 6 )  ) {
				$dateformatstring = "j";
				$unixtimestamp = strtotime(get_field('start_date'));
				echo "<div>" , date_i18n( $dateformatstring, $unixtimestamp ), " - ";
				$dateformatstring = "j F Y";
				$unixtimestamp = strtotime(get_field('end_date'));
				echo date_i18n( $dateformatstring, $unixtimestamp ) , "</div>";

			// start en einddatum zelfde jaar
			} else  if ( substr( get_field( 'start_date' ), 0, 4 ) == substr( get_field( 'end_date' ), 0, 4 )  ) {
				$dateformatstring = "j F";
				$unixtimestamp = strtotime(get_field('start_date'));
				echo "<div>" , date_i18n( $dateformatstring, $unixtimestamp ), " - ";
				$dateformatstring = "j F Y";
				$unixtimestamp = strtotime(get_field('end_date'));
				echo date_i18n( $dateformatstring, $unixtimestamp ) , "</div>";

			// start en einddatum verschillend jaar
			} else {
				$dateformatstring = "j F Y";
				$unixtimestamp = strtotime(get_field('start_date'));
				echo "<div>" , date_i18n( $dateformatstring, $unixtimestamp ), " - ";
				$dateformatstring = "j F Y";
				$unixtimestamp = strtotime(get_field('end_date'));
				echo date_i18n( $dateformatstring, $unixtimestamp ) , "</div>";
			}

			if ( ! trim( get_field( 'location_name' ) ) == false ) {
				echo "<div>" , the_field('location_name') , "</div>";
			}

			if ( ( ! trim( get_field( 'location_city' ) ) == false ) AND ( get_field( 'location_city' ) !== 'Amsterdam' ) ) {
				echo "<div>" , the_field( 'location_city' ) , "</div>";
			}

			if ( ! trim( get_field( 'frontpage_text' ) ) == false ) {
				echo the_field( 'frontpage_text' );
			} else {
				echo "<p></p>";
			}

		endwhile;

		wp_reset_query();

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'amstelodamum' );
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // Class Activity_Widget

// Register Activity_Widget widget
function register_activity_widget() {
    register_widget( 'Activity_Widget' );
}

add_action( 'widgets_init', 'register_activity_widget' );


/**
 * Add Publication_Widget widget
 */
class Publication_Widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'publication_widget', // Base ID
			__( 'Publicaties', 'amstelodamum' ), // Name
			array( 'description' => __( 'Publication Widget', 'amstelodamum' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}

		$today = date( 'Ymd' );

		$query_args = array(
			'post_type' => 'publication',
			'posts_per_page' => 2,
			'order' => 'DESC',
		);

		$the_query = new WP_Query( $query_args );

		while ($the_query -> have_posts()) : $the_query -> the_post();

			$publication_cover = get_field('publication_cover');

			if ( $publication_cover ) :
				echo '<div class="publication-widget-cover">', '<a href="', the_permalink() , '" >' , wp_get_attachment_image( $publication_cover, 'small' ), '</div>';
			endif;

		endwhile;

		wp_reset_query();

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'amstelodamum' );
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // Class Publication_Widget

// Register Publication_Widget widget
function register_publication_widget() {
    register_widget( 'Publication_Widget' );
}

add_action( 'widgets_init', 'register_publication_widget' );

function amstelodamum_build_svg( $svg_src, $fallback = false) {
	if ( ! is_string( $svg_src ) || '' === $svg_src ) {
		return;
	}
	if ( ! is_readable( $svg_src ) ) {
		return;
	}
	$svg = file_get_contents( $svg_src );
	if ( $fallback ) {
		$png_src = str_replace( 'svg', 'png', $svg_src );
		$svg = amstelodamum_insert_svg_fallback( $svg, $png_src );
	}
	return $svg;
}

function amstelodamum_insert_svg_fallback( $svg, $png_src ) {
	// Add png fallback if available.
	if ( ! is_readable( $png_src ) ) {
		return $svg;
	} else {
		$png_insert = '<image class="svg-fallback" src="' . $png_src . '" xlink:href=""></svg>';
		$svg = substr_replace( $svg, $png_insert, -6 );
		return $svg;
	}
}

add_filter('pre_get_posts', 'amstelodamum_more_search_results', 10, 1);

function amstelodamum_more_search_results( $query ) {
    if( $query->is_search ){
        $query->set( 'posts_per_page', 5 );
    }
    return $query;
}
